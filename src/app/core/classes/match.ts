export class Match{
    Date;
    HomeTeam;
    AwayTeam;
    Prediction;

    constructor(Date, HomeTeam, AwayTeam, Prediction:number=0) {
        this.Date = Date;
        this.HomeTeam = HomeTeam;
        this.AwayTeam = AwayTeam;
        this.Prediction = Prediction;
    }
}