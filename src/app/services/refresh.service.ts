import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { Match } from "src/app/core/classes/match";

@Injectable()
export class RefreshService {
    private refresh = new Subject<Match>();

    refreshObservable = this.refresh.asObservable();

    updateMatch(match) {
        this.refresh.next(match);
    }
}