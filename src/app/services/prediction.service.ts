import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, forkJoin } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictionService {
  readonly baseURL    = "https://sample.hu";
  readonly token      = "asdfsdfasdfa";
  private prediction  = "/prediciton";

  header;

  constructor(private http: HttpClient) { 
    //TODO
  }

  getPrediction() {
    var dummyPredictions = [
      {
        "date": "2020.04.10",
        "homeTeam": "Arsenal",
        "awayTeam": "Chelsea",
        "prediction": "H"
      },
      {
        "date": "2020.04.11",
        "homeTeam": "MAN UTD",
        "awayTeam": "MAN City",
        "prediction": "H"
      },
      {
        "date": "2020.04.12",
        "homeTeam": "Bolton",
        "awayTeam": "Leeds UTD",
        "prediction": "V"
      },
      {
        "date": "2020.04.13",
        "homeTeam": "West Ham UTD",
        "awayTeam": "Chelsea",
        "prediction": "H"
      },
      {
        "date": "2020.04.14",
        "homeTeam": "Liverpool",
        "awayTeam": "Aston Villa",
        "prediction": "V"
      }
    ]

    return dummyPredictions;
    /* 
    this.header = this.addTokenToHeader();
    return this.http.get(this.baseURL + this.prediction, { headers: this.header}).pipe(
      retry(1),
      catchError(this.handleError)
    ) */
  }

  addTokenToHeader() {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('X-Access-Token', this.token);

    return headers;
  }

  handleError(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      //client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      //server-side error
      errorMessage = error.error.Message;
      if (errorMessage == null || errorMessage == "") {
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      }      
    }

    return throwError(errorMessage);
  }
}
