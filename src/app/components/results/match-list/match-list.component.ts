import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PredictionService } from 'src/app/services/prediction.service';

@Component({
  selector: 'match-list',
  templateUrl: './match-list.component.html',
  styleUrls: ['./match-list.component.scss']
})

export class MatchListComponent implements OnInit {
  matchList$: Observable<
    Array<{
      date: string;
      homeTeam: string;
      awayTeam: string;
      prediction: string;
    }>
  >;

  constructor(private service: PredictionService) {}

  ngOnInit(): void {
    this.matchList$ = this.service.getPrediction();
  }
} 