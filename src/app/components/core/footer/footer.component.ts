import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  giveFeedback  =   "Give me a feedback! "
  email         =   "neczpal[dot]oliver[at]outlook[dot]hu"
  copy          =   "copy"
  copied        =   "copied"

  constructor() { }

  ngOnInit(): void {
  }

}
